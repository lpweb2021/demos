/** Speech To Text */

const fs = require('fs');
const speech = require('@google-cloud/speech').v1p1beta1;
// Creates a client
const apikey = { projectId : 'PROJECT',	keyFilename : './key.json'};
const client = new speech.SpeechClient (apikey);

/**
 * transcribe an audio file
 * @param  {object} audio 
 * @param  {object} config 
 * @return transcription
 */
function transcribe(audio, config)
{
    // Verify config
    config = config || {};
    config.audioChannelCount = config.audioChannelCount || 1;
	config.enableAutomaticPunctuation = config.enableAutomaticPunctuation || true;
    config.enableSeparateRecognitionPerChannel = config.enableSeparateRecognitionPerChannel || true;
    config.enableSpeakerDiarization =  config.enableSpeakerDiarization || true;
    config.diarizationSpeakerCount = config.diarizationSpeakerCount || 10;
	config.enableWordConfidence = config.enableWordConfidence || true;
	config.encoding = config.encoding || 'LINEAR16';
	config.sampleRateHertz = config.sampleRateHertz || 8000;
	config.languageCode	= config.languageCode || 'fr-FR' ;
	config.maxAlternatives = config.maxAlternatives || 1;
	config.enableWordTimeOffsets = config.enableWordTimeOffsets || true;
    config.speechContexts = config.speechContexts || [];

    const request = { audio: audio,
                      config: config };

    // console.dir(request.config);

    // Detects speech in the audio file
    return new Promise(async function (resolve)
	{
        const [response] = await client.recognize(request);
        const transcription = response.results.map(result => result.alternatives[0].transcript).join('\n');
        resolve(transcription);    
    });
}

exports.transcribe = transcribe