/** Text To Speech */

const fs = require('fs');

// Imports the Google Cloud client library
const textToSpeech = require('@google-cloud/text-to-speech').v1beta1;

// Creates a client
const apikey = { projectId : 'PROJECT_NAME',	keyFilename : './key.json'};
const client = new textToSpeech.TextToSpeechClient (apikey);


/**
 * generate audio data from text using text to speech
 * @param {string} text 
 * @param {string} language 
 * @param {object} voice
 *      @param {float}  voice.name
 *      @param {float}  voice.pitch  
 *      @param {float}  voice.rate
 * @param {object} format 
 *      @param {integer} format.sampleRate
 *      @param {string}  format.encoding
 *      @param {integer} format.gain
 */
function speak(text, language, voice, format)
{
    console.log('speak', 'lang', language, 'voice', voice, 'text', text)
    format = format || {};
    voice  = voice  || {};
    var audio = false;

    return new Promise( async (resolve) => 
	{                
        if(text && text.length)
        {
            const request = 
            {
                input       : { ssml            : '<speak>'+text+'</speak>'},
                voice       : { languageCode    : language || 'fr-FR', 
                                name            : voice.name ||  'fr-FR-Wavenet-E' },
                audioConfig : { audioEncoding   : format.encoding || 'LINEAR16', 
                                sampleRateHertz : format.sampleRate || 8000, 
                                volumeGainDb    : format.gain || 0,
                                pitch           : voice.pitch || "0.00",
				                speakingRate    : voice.rate || "1.10"  }
            };

            client.synthesizeSpeech(request, (err, response) => 
            {
                if(errr)    console.log('ERREUR !', err.message);
                else        audio = response.audioContent; 
            });    
        }
        resolve (audio);
    });
}

/**
 * generate audio data from text using text to speech
 * @param {string} filename
 * @param {string} text 
 * @param {string} language 
 * @param {object} voice
 *      @param {float}  voice.name
 *      @param {float}  voice.pitch  
 *      @param {float}  voice.rate
 * @param {object} format 
 *      @param {integer} format.sampleRate
 *      @param {string}  format.encoding
 *      @param {integer} format.gain
 */
function speakToFile(filename, text, language, voice, format)
{
    return new Promise(async (resolve) =>
	{
        let saved = false;                
	    let audio = await speak(text, language, voice, format);
        if(audio)
        {
            fs.writeFile(filename, audio, (err) => 
            {
                if(err)     console.log('ERREUR !', err.message);
                else        saved = true;
            });
        }
        else                console.log('ERREUR !', 'Aucun audio généré par la synthèse !');
        resolve (saved);
    });
}

exports.speak = speak;
exports.speakToFile = speakToFile;
